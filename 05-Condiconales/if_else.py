""" 
# condicional if
SI se cumple esta condicion
 se ejecuta un grupo de condiciones
SI NO:
 se ejecuta otro grupo de condiciones

if condicion:
    intrucciones
else:
    otras instrucciones

# operadoes de comparacion
== igual
!= diferente
< menor que 
> mayor que
<= menor o igual que 
>= mayor o igual que

"""

# ejemplo 1

print("###### ejemplo 1 ########")

color = input("adivina cual es mi color favorito:")

if color == "rojo":
    print("en hora buena !!!")
    print("el color es rojo")

else:
    print("color incorrecto!!")

# ejemplo 2

print("###### ejemplo 2 ########")

year = 2020
year =  int(input("¿en que año estamos?"))

if year >= 2021:
    print(" estamos de 2021 en adelante")
else:
    print("Es un año anterior a 2021")

 # ejemplo 3

print("\n###### ejemplo 3 ########")

nombre = "jhony cisneros"
ciudad = "lima"
continente = "sudamerica"
edad = 27
mayoria_edad = 18

if edad >= mayoria_edad:
    print(f"{nombre} Es mayor de edad !!!")

    if continente !="sudamerica":
        print("el usuario no es sudamericano")
    
    else:
        print(f"Es sudamericano y de la ciudad de {ciudad}")


else:
    print(f"{nombre} NO es mayor de edad")