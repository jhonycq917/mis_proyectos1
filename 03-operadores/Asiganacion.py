# operadores de asignacion
edad = 55
print(edad)

edad += 5

print(edad)


# operadores de incremento y decremento

año = 2022

# incremento
año = año + 1
año += 1
año -= 2

# decremento
año = año - 1
print(año)

