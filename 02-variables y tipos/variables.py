"""
una variable es un contenedor de informacion que dentro
guardara un dato, se pueden crear mucgas variables y que
cada una tenga dato Distinto
"""

texto = "'master en python'"
texto2 ="jhony cisneros"
print(texto)
print(texto2)

print("-----------------")
numero = 10
print(numero)
print(type(numero))

numero = 10.57
print(numero)
print(type(numero))

cadena = "estoy 'estudiando'"
print(cadena)
print(type(cadena))

# verdadero o falso
Valor = True
print(Valor)
print(type(Valor))

Valor = False
print(Valor)
print(type(Valor))

num1 = 10
num2 = 6.7
resultado = (num1 + num2) * 10/6
print('el resultado es:',resultado)

# concatenacion
nombre = "jhony"
apellido = "cisneros"
linkedin = "ingecons"
suma = nombre + " " + apellido + "-" + linkedin
print(suma)